﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.bookdemo
{
    class Histogram1D
    {
        int histSize = 256;
        float lowLimit = 0;
        float upLimit = 255;
        int channelIndex = 0;
        Rangef rangef = new Rangef(0, 155);

       public Mat GetHistorgram(Mat img)
        {
            Mat[] mats = new Mat[] { img };//一张图片，初始化为panda
            Mat hist = new Mat();//用来接收直方图
            int[] channels = new int[] { 0 };//一个通道,初始化为通道0
            int[] histsize = new int[] { 256 };//一个通道，初始化为256箱子
            Rangef[] range = new Rangef[1];//一个通道，值范围
            range[0].Start = 0.0F;//从0开始（含）
            range[0].End = 256.0F;//到256结束（不含）
            Mat mask = new Mat();//不做掩码
            Cv2.CalcHist(mats, channels, mask, hist, 1, histsize, range);//计算灰度图，dim为1 1维
 
            return hist;
        }
        public List<float>  GetHistorgramFloatArr(Mat img)
        {
           var outMat= GetHistorgram(img);
            List<float> res = new List<float>(256);
            for (int i = 0; i < 256; i++)
            {
                res.Add(outMat.At<float>(i));
            }
            return res;
        }
    }
}
