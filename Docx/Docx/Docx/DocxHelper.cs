﻿using System.Drawing;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace Docx
{
    public static class DocxHelper
    {

        public static void InsertParagraph(this DocX document, string content, HeadingType level)
        {
           
                Paragraph pTitle = document.InsertParagraph();
                pTitle.Append(content).Heading((HeadingType)level).Color(Color.Black);
                document.Save();
           
        }


        public static void insertParagraph(string docx, string content)
        {
            using (DocX document = DocX.Load(docx))
            {

                Paragraph pTitle = document.InsertParagraph();
                pTitle.Append(content);

                document.Save();
            }
        }
        public static Paragraph Heading(this Paragraph paragraph, int level)
        {
            paragraph.StyleName = "Heading" + level.ToString();
            paragraph.ListItemType = ListItemType.Numbered;
            return paragraph;
        }
    }
}