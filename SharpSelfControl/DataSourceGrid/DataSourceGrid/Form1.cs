﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataSourceGrid
{
    public partial class Form1 : Form
    {
        public enum  Statu
        {
            Normal,
            Error,
      
       }

        public class Card
        {
            public Statu Statu { get; set; } = Statu.Error;
        }
        List<Card> cards=new List<Card>();
        public Form1()
        {
            InitializeComponent();
            cards.Add(new Card());
            cards.Add(new Card());
            cards.Add(new Card());
            this.dataGridView1.DataSource = cards;
            BindingSource bs = new BindingSource();
            bs.DataSource = new Dictionary<Statu, string>()
            {
                { Statu.Normal, "正常"},
                {Statu.Error, "故障"},
            };
            DataGridViewComboBoxColumn dgvCmb=new DataGridViewComboBoxColumn();
            dgvCmb.ValueType = typeof(Statu);
            dgvCmb.DisplayMember = "Value";
            dgvCmb.ValueMember = "Key";
            dgvCmb.DataPropertyName = "Statu";
            dgvCmb.DataSource = bs;
           
            this.dataGridView1.Columns.Add(dgvCmb);
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

        }
    }
}
