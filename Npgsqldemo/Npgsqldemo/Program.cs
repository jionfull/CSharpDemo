﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Npgsqldemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string ConStr = @"PORT=5432;DATABASE=testdb;HOST=localhost;PASSWORD=P@ssw0rd;USER ID=postgres";
            NpgsqlConnection sqlConn = new NpgsqlConnection(ConStr);
            sqlConn.Open();
            DataSet ds = new DataSet();
            String sqlstr = "SELECT * FROM public.demo";
            try
            {
                using (NpgsqlDataAdapter sqldap = new NpgsqlDataAdapter(sqlstr, sqlConn))
                {
                    sqldap.Fill(ds);
                }
             
            }
            catch (System.Exception ex)
            {
               
               
            }
        }
    }
}
