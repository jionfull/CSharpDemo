﻿using FaceRecognitionDotNet;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceRegionDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
           
           var _FaceRecognition = FaceRecognition.Create(".\\Source");
            var testImages = new[]
             {
               
                    "yangmi1.jpg",
                    "yangmi3.jpg",
                };
            using (var mat = Cv2.ImRead("yangmi2.jpg"))
            {
                var bytes = new byte[mat.Rows * mat.Cols * mat.ElemSize()];
                Marshal.Copy(mat.Data, bytes, 0, bytes.Length);

                using (var searchImage = FaceRecognition.LoadImage(bytes, mat.Rows, mat.Cols, mat.ElemSize()))
                {
                    var searchEncodings = _FaceRecognition.FaceEncodings(searchImage);

                    foreach (var path in testImages)
                    {
                        var targetImage = FaceRecognition.LoadImageFile(path);
                        var targetEncoding = _FaceRecognition.FaceEncodings(targetImage);

                        var distance = FaceRecognition.FaceDistance(searchEncodings.First(), targetEncoding.First());
                        Console.WriteLine($"Distance: {distance} for {path}");

                        foreach (var encoding in targetEncoding) encoding.Dispose();
                    }

                    foreach (var encoding in searchEncodings) encoding.Dispose();
                }
            }

        }
    }
}
