﻿using OpenCvSharp;
using OpenCvSharp.Dnn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.bookdemo
{
    class HistogramColor
    {
        int[] channels = new int[] { 0, 1, 2 };//一个通道,初始化为通道0
        int[] histsize = new int[] { 256 ,256,256};//一个通道，初始化为256箱子
        Rangef[] range = new Rangef[3];//一个通道，值范围

        public HistogramColor()
        {
            for (int i = 0; i < range.Length; i++)
            {
                range[i].Start = 0.0F;//从0开始（含）
                range[i].End = 256.0F;//到256结束（不含）
            }
        }

        public Mat GetHistorgram(Mat img)
        {
            Mat[] mats = new Mat[] { img };//一张图片，初始化为panda
            Mat hist = new Mat();//用来接收直方图

           
            Mat mask = new Mat();//不做掩码
            Cv2.CalcHist(mats, channels, mask, hist, 3, histsize, range);//计算灰度图，dim为1 1维
            
            return hist;
        }
        public List<List<float>> GetHistorgramFloatArr(Mat img)
        {
            var outMat = GetHistorgram(img);
            List<List<float>> res = new List<List<float>>(3);
            for (int i = 0; i < 3; i++)
            {
                List<float> chan = new List<float>();
                    for (int j = 0; j < 256; j++)
                {
                    chan.Add(outMat.At<float>(i,j));
                }
                res.Add(chan);
            }
            
            return res;
        }
    }
}
