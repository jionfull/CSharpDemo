﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace Docx
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var path = "new.docx";
            var imagePath = "abc.png";
            CreateDoc(path, imagePath);
        }

        private static void CreateDoc(string path, string imagePath)
        {
            using (var document = DocX.Create(path))
            {
                // 在文档中添加超链接。
                var link = document.AddHyperlink("link", new Uri("http://www.google.com"));
                // 在文档中添加一个表。
                var table = document.AddTable(2, 2);
                table.Design = TableDesign.ColorfulGridAccent2;
                table.Alignment = Alignment.center;
                table.Rows[0].Cells[0].Paragraphs[0].Append("1");
               
                table.Rows[0].Cells[1].Paragraphs[0].Append("2");
                table.Rows[1].Cells[0].Paragraphs[0].Append("3");
                table.Rows[1].Cells[1].Paragraphs[0].Append("4");
                var newRow = table.InsertRow(table.Rows[1]);
                newRow.ReplaceText("4", "5");
                // 将图像添加到文档中。    
                var image = document.AddImage(imagePath);
                //创建一个图片（一个自定义视图的图像）。
                var picture = image.CreatePicture();
                picture.Rotation = 10;
                picture.SetPictureShape(BasicShapes.cube);
                // 在文档中插入一个新段落。
                var title = document.InsertParagraph().Append("Test").FontSize(20).Font("Comic Sans MS");
                title.Alignment = Alignment.center;
                // 在文档中插入一个新段落。
                var p1 = document.InsertParagraph();
                // 附加内容到段落
                p1.AppendLine("This line contains a ").Append("bold").Bold().Append(" word.");
                p1.AppendLine("Here is a cool ").AppendHyperlink(link).Append(".");
                p1.AppendLine();
                p1.AppendLine("Check out this picture ").AppendPicture(picture).Append(" its funky don't you think?");
                p1.AppendLine();
                p1.AppendLine("Can you check this Table of figures for me?");
                p1.AppendLine();
               
                // 在第1段后插入表格。
                p1.InsertTableAfterSelf(table);
                // 在文档中插入一个新段落。
                Paragraph p2 = document.InsertParagraph();
                // 附加内容到段落。
                p2.AppendLine("Is it correct?");
                // 保存当前文档
                document.Save();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (DocX document = DocX.Load("new.docx"))
            {
                //文本替换
                document.ReplaceText("AAA", "BBB");
                //表格操作
                var table = document.Tables[0];

                document.SaveAs("replace.docx");
            }
        }
        //using (var document = DocX.Load(DocumentSample.DocumentSampleResourcesDirectory + @"ReplaceText.docx"))
            //{
            //    // Check if some of the replace patterns are used in the loaded document.
            //    if (document.FindUniqueByPattern(@"<[\w \=]{4,}>", RegexOptions.IgnoreCase).Count > 0)
            //    {
            //        // Do the replacement of all the found tags and with green bold strings.
            //        document.ReplaceText("<(.*?)>", DocumentSample.ReplaceFunc, false, RegexOptions.IgnoreCase, new Formatting() { Bold = true, FontColor = System.Drawing.Color.Green });

            //        // Save this document to disk.
            //        document.SaveAs(DocumentSample.DocumentSampleOutputDirectory + @"ReplacedText.docx");
            //        Console.WriteLine("\tCreated: ReplacedText.docx\n");
            //    }

            //}
    }
}
