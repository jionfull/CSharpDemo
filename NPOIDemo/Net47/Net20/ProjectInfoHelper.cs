﻿using System;
using IniParser.Model;

namespace NPOIDEMO
{
    public static class ProjectInfoHelper
    {
         public static void ReadFormIni(this ProjectInfo projectInfo, String filePath)
        {
            var parser = new IniParser.FileIniDataParser();

            IniData ini = parser.ReadFile(filePath);
            var sec=ini.Sections["项目配置"];
          
            projectInfo.Name = sec["项目名称"]?? "2018版铁路信号集中监测系统";
            projectInfo.ProjectId = sec["项目编号"]?? "LONPM.LYF2018001.YF01";
            

        }
    }
}