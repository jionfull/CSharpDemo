﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PythonPlotter;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = Enumerable.Range(0, 200).Select(ia => (double)ia / 100.0);
            var y = x.Select(ia => Math.Sin(2.0 * ia * Math.PI));
            var plotter=new Plotter();
            plotter.Python = "C:\\Users\\Administrator\\Anaconda3\\python.exe";
          
            plotter.Plot();
           // plotter.Plot(x, y, "Test figure", "$x$", @"$\sin(2 \pi x)$"); 
        }
    }
}
