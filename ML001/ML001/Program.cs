﻿using ML001ML.Model;
using System;

namespace ML001
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一句英文进行预测");
            //定义输入
            var input = new ModelInput();

            //获取用户输入
            string userInput = Console.ReadLine();
            input.Wow____Loved_this_place_ = userInput;

            // 加载模型，输出预测结果。
            ModelOutput result = ConsumeModel.Predict(input);

            Console.WriteLine($"用户输入: {input.Wow____Loved_this_place_}\n预测结果: {result.Prediction}");
        }
    }
}
